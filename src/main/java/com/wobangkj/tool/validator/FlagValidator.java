package com.wobangkj.tool.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 用户验证状态是否在指定范围内的注解
 * Created by macro on 2018/4/26.
 *
 * @author cliod
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(validatedBy = FlagValidatorClass.class)
public @interface FlagValidator {
    /**
     * 有效值
     *
     * @return 数组
     */
    String[] value() default {};

    /**
     * 无效信息
     *
     * @return 字符串
     */
    String message() default "flag is not found";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
