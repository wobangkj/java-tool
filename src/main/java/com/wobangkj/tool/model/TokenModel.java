package com.wobangkj.tool.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * token model
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenModel {
	// 兼容id
	private Long uid;
	// 用户 id
	private Long id;
	// 随机生成的 uuid
	private String token;

	public TokenModel(Long id, String token) {
		this.id = id;
		this.uid = id;
		this.token = token;
	}
}
