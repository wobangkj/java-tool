package com.wobangkj.tool.model.wx;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 退款model 详见 https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_4
 *
 * @author dreamlu
 * @date 2019/06/03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Refund {

	private Integer total_fee;

	private String appid;

	private String out_trade_no;

	private Integer refund_fee;

	private String refund_desc;
}
