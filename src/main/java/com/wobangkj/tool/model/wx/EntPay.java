package com.wobangkj.tool.model.wx;

import lombok.Data;

/**
 * @author dreamlu
 * @date 2019/06/13
 */
@Data
public class EntPay {

	// 提现类型,0零钱,1银行卡
	private Integer type;

	// appId
	private String appId;

	// openid
	private String openId;

	// 提现金额
	private Integer amount;

	// 银行卡号
	private String encBankNo;

	// 收款人姓名
	private String encTrueName;

	// 银行编号
	private String bankCode;
}