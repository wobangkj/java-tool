package com.wobangkj.tool.util;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.util.HashSet;
import java.util.Set;

import static com.wobangkj.tool.util.BeanUtil.getStrings;

/**
 * 对象合并
 */
@Deprecated
public class RestUtil {

	/**
	 * value 重复,src 覆盖 target, 其余合并
	 * @param src
	 * @param target
	 */
	public static void copyNonNullProperties(Object src, Object target) {
		BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
	}

	public static String[] getNullPropertyNames(Object source) {
		final BeanWrapper src = new BeanWrapperImpl(source);
		java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

		Set<String> emptyNames = new HashSet<String>();
		return getStrings(src, pds, emptyNames);
	}
}
