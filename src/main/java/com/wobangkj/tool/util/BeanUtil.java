package com.wobangkj.tool.util;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Bean工具类
 *
 * @author cliod
 * @date 19-7-18
 */
public class BeanUtil {

	/**
	 * 对象是否为空
	 *
	 * @param obj 对象
	 * @param <T> 对象类型
	 * @return 是否为空
	 * @see Objects#isNull(Object)
	 */
	@Deprecated
	public static <T> boolean isNull(T obj) {
		return Objects.isNull(obj);
	}

	/**
	 * 判断对象以及内容是否为空
	 * <li>只要有一个不为空即不为空</li>
	 *
	 * @param obj 对象
	 * @return 是否为空
	 */
	public static <T> boolean isObjEmpty(T obj) {
		if (Objects.isNull(obj)) {
			return false;
		}
		try {
			if (obj instanceof String) {
				return ((String) obj).isEmpty();
			}
			Field[] fields = obj.getClass().getDeclaredFields();
			for (Field field : fields) {
				field.setAccessible(true);
				//判断字段是否为空，并且对象属性中的基本都会转为对象类型来判断
				if (Objects.nonNull(field.get(obj))) {
					return false;
				}
			}
			return true;
		} catch (IllegalAccessException e) {
			return false;
		}
	}

	/**
	 * src 覆盖 target, 其余合并
	 *
	 * @param src    源
	 * @param target 目标
	 */
	public static void copyNonNullProperties(Object src, Object target) {
		BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
	}

	/**
	 * 获取对象中不为空的字段
	 *
	 * @param obj 对象
	 * @return 字段值数组
	 */
	public static String[] getNullPropertyNames(Object obj) {
		final BeanWrapper src = new BeanWrapperImpl(obj);
		java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

		Set<String> emptyNames = new HashSet<>();
		return getStrings(src, pds, emptyNames);
	}

	static String[] getStrings(BeanWrapper src, PropertyDescriptor[] pds, Set<String> emptyNames) {
		for (PropertyDescriptor pd : pds) {
			Object srcValue = src.getPropertyValue(pd.getName());
			if (srcValue == null) {
				emptyNames.add(pd.getName());
			}
		}
		String[] result = new String[emptyNames.size()];
		return emptyNames.toArray(result);
	}

	/**
	 * 获取对象字段值
	 *
	 * @param obj 对象
	 * @param key 字段
	 * @return 值
	 */
	public static Object getFieldValue(Object obj, String key) throws ReflectiveOperationException {
		Field field = obj.getClass().getDeclaredField(key);
		field.setAccessible(true);
		return field.get(obj);
	}

	/**
	 * 给对象的属性值赋值
	 * 注: 暂无反射删除方法
	 *
	 * @param key   字段名
	 * @param value 字段值
	 * @param obj   操作对象
	 * @return 是否成功赋值
	 */
	public static boolean setFieldValue(Object obj, String key, Object value) {
		try {
			Field field = obj.getClass().getDeclaredField(key);
			//设置对象的访问权限，保证对private的属性的访问
			field.setAccessible(true);
			field.set(obj, value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static String[] getFieldNames(Object obj) {
		Field[] fields = obj.getClass().getFields();

		String[] result = new String[fields.length];
		for (int i = 0; i < fields.length; i++) {
			result[i] = fields[i].getName();
		}
		return result;
	}

	@Deprecated
	public static String getFieldNames(Class<?> obj) {
		Field[] fields = obj.getFields();
		StringBuffer sb = new StringBuffer();

		for (Field field : fields) {
			sb.append(field.getName());
			sb.append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}

	@Deprecated
	public static <T> T newInstance(Class<T> clazz) {
		try {
			return clazz.newInstance();
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}
}
