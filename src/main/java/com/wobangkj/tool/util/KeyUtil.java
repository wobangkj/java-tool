package com.wobangkj.tool.util;

import org.apache.commons.codec.binary.Base64;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * @author Cliod
 * @date 2019/6/2
 */
public class KeyUtil {

	/**
	 * 随机数
	 *
	 * @param len 长度
	 * @return 随机数
	 */
	public static String randNum(Integer len) {
		return String.valueOf((int) ((Math.random() * 9 + 1) * Math.pow(10, len - 1)));
	}

	/**
	 * 解密 utf8
	 *
	 * @param value 待解密字符
	 * @return 解密字符
	 */
	public static String decode(String value) {
		return new String(Base64.decodeBase64(value.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
	}

	/**
	 * SHA-265 机密
	 *
	 * @param value 字符串
	 * @return 加密结果
	 */
	public static String encrypt(String value) throws NoSuchAlgorithmException {
		String encodeStr = "";
		if (isEmpty(value)) {
			return encodeStr;
		}
		MessageDigest messageDigest;
		messageDigest = MessageDigest.getInstance("SHA-256");
		messageDigest.update(value.getBytes(StandardCharsets.UTF_8));
		encodeStr = byte2Hex(messageDigest.digest());
		return encodeStr;
	}

	/**
	 * MD5 计算
	 *
	 * @param value 字符串
	 * @return MD5值
	 */
	public static String md5(String value) {
		return Arrays.toString(DigestUtils.md5Digest(value.getBytes(StandardCharsets.UTF_8)));
	}

	/**
	 * 将byte转为16进制
	 *
	 * @param bytes 字符byte
	 * @return 加密字符串
	 */
	private static String byte2Hex(byte[] bytes) {
		StringBuilder stringBuffer = new StringBuilder();
		String temp;
		for (byte b : bytes) {
			temp = Integer.toHexString(b & 0xFF);
			if (temp.length() == 1) {
				// 1得到一位的进行补0操作
				stringBuffer.append("0");
			}
			stringBuffer.append(temp);
		}
		return stringBuffer.toString();
	}

	/**
	 * base64 加密 utf8
	 *
	 * @param value 待加密字符
	 * @return 加密字符
	 */
	public static String encode(String value) {
		if (isEmpty(value)) {
			return "";
		}
		return new String(Base64.encodeBase64(value.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
	}

	private static boolean isEmpty(String value) {
		return null == value || "".equals(value);
	}

	/**
	 * 生成长 id
	 *
	 * @return id
	 */
	public static String generateTimeKey() {
		return System.currentTimeMillis() + generateIntKey();
	}

	/**
	 * 6位数字code
	 *
	 * @return code
	 */
	public static String generateIntKey() {
		return randNum(6);
	}
}
