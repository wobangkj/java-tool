package com.wobangkj.tool.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 列表转化为Map,通过id和对象映射
 *
 * @author cliod
 * @date 2019/7/26
 * package : com.wobangkj.util
 */
public class List2MapUtil {

	/**
	 * 将list转化为map
	 * 指定字段为key,list中的对象为value
	 *
	 * @return
	 */
	public static <T> Map<String, T> convert(List<T> list, String key) throws ReflectiveOperationException {
		Map<String, T> map = new HashMap<>(16);
		Object obj = null;
		for (T t : list) {
			if (Objects.isNull(obj)) {
				obj = BeanUtil.getFieldValue(t, key);
			}
			if (Objects.isNull(obj)) {
				return map;
			}
			map.put(obj.toString(), t);
		}
		return map;
	}

	/**
	 * 将list转化为map
	 * 指定list中对象的字段作为map的key和value
	 *
	 * @return
	 */
	public static <T> Map<String, Object> convert(List<T> list, String keyName, String valueName) throws ReflectiveOperationException {
		Map<String, Object> map = new HashMap<>(16);
		Object obj = null;
		for (Object t : list) {
			if (Objects.isNull(obj)) {
				obj = BeanUtil.getFieldValue(t, keyName);
			}
			if (Objects.isNull(obj)) {
				return map;
			}
			map.put(obj.toString(), BeanUtil.getFieldValue(t, valueName));
		}
		return map;
	}
}
