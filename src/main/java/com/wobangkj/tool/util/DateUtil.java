package com.wobangkj.tool.util;

import java.text.*;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具
 *
 * @author cliod
 * @date 19-7-19
 * com.wobangkj.git.magicked.util
 */
@Deprecated
public class DateUtil {

    public static final String FORMAT_DAY = "yyyyMMdd";
    public static final String FORMAT_DAY_BAR = "yyyy-MM-dd ";
    public static final String FORMAT_SECOND = "HHmmss";
    public static final String FORMAT_SECOND_COLON = "HH:mm:ss";

    /**
     * 获取当时间的周一
     *
     * @param date 时间
     * @return 周一
     */
    public static Date getThisWeekMonday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        // 获得当前日期是一个星期的第几天
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
        if (1 == dayWeek) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        // 设置一个星期的第一天
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        // 获得当前日期是一个星期的第几天
        int day = cal.get(Calendar.DAY_OF_WEEK);
        // 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
        cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
        return cal.getTime();
    }

    /**
     * 获取时间戳
     *
     * @return 时间戳字符串
     */
    public static String getTimestampStr() {
        return Long.toString(getTimestampLong());
    }

    /**
     * 获取时间戳
     *
     * @return 时间戳
     */
    public static Long getTimestampLong() {
        return System.currentTimeMillis();
    }

    /**
     * 获取时间
     * 格式:yyyyMMdd
     *
     * @return 时间字符串
     */
    public static String getDateTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DAY + FORMAT_SECOND);
        return sdf.format(new Date(System.currentTimeMillis()));
    }

    /**
     * 获取时间
     * 格式:yyyyMMdd
     *
     * @return 时间
     */
    public static Long getDateTimeLong() {
        return Long.parseLong(getDateTimeStr());
    }

    public static String getDateStr() {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DAY);
        return sdf.format(new Date(System.currentTimeMillis()));
    }

    public static Long getDateLong() {
        return Long.parseLong(getDateStr());
    }


    public static String getTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_SECOND);
        return sdf.format(new Date(System.currentTimeMillis()));
    }

    public static Long getTimeLong() {
        return Long.parseLong(getTimeStr());
    }

    /**
     * 时间戳转时间
     *
     * @param timestamp 时间戳
     * @return 时间字符串
     */
    public static String convertToDateTimeStr(String timestamp) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DAY_BAR + FORMAT_SECOND_COLON);
        return sdf.format(new Date(Long.parseLong(timestamp)));
    }

    /**
     * 时间戳转时间
     *
     * @param timestamp 时间戳
     * @return 时间
     */
    public static Date convertToDateTime(String timestamp) {
        return new Date(Long.parseLong(timestamp));
    }

    /**
     * 时间转时间戳
     *
     * @param dateTime 时间
     * @return 时间戳字符串
     */
    public static String convertToTimestampStr(String dateTime) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DAY_BAR + FORMAT_SECOND_COLON);
        ParsePosition position = new ParsePosition(0);
        return Long.toString(sdf.parse(dateTime, position).getTime());
    }

    /**
     * 时间转时间戳
     *
     * @param dateTime 时间
     * @return 时间戳
     */
    public static Long convertToTimestampLong(String dateTime) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DAY_BAR + FORMAT_SECOND_COLON);
        ParsePosition position = new ParsePosition(0);
        return sdf.parse(dateTime, position).getTime();
    }

    /**
     * 时间转日期
     *
     * @param time 时间
     * @return 日期
     */
    public static String convertDateTimeStr(String time) {
        String result;
        if (time.length() == FORMAT_DAY.length() || time.length() == (FORMAT_DAY + FORMAT_SECOND).length()) {
            result = time.substring(0, 4) + "-" +
                    time.substring(4, 6) + "-" +
                    time.substring(6, 8);
            if (time.length() == (FORMAT_DAY + FORMAT_SECOND).length()) {
                result += " " +
                        time.substring(8, 10) + ":" +
                        time.substring(10, 12) + ":" +
                        time.substring(12, 14);
            }
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DAY + FORMAT_SECOND);
            Date date = null;
            try {
                date = sdf.parse(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            sdf = new SimpleDateFormat(FORMAT_DAY_BAR + FORMAT_SECOND_COLON);
            result = sdf.format(date);
        }
        return result;
    }

    /**
     * 时间相减得到天数
     *
     * @param beginDateStr 开始时间
     * @param endDateStr   结束时间
     * @return long
     * @author Administrator
     */
    public static long getDaySub(String beginDateStr, String endDateStr) {
        long day = 0;
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_DAY_BAR);
        Date beginDate;
        Date endDate;
        try {
            beginDate = format.parse(beginDateStr);
            endDate = format.parse(endDateStr);
            day = (endDate.getTime() - beginDate.getTime()) / (24 * 60 * 60 * 1000);
        } catch (Exception e) {
            return -1;
        }
        return day;
    }

}
