package com.wobangkj.tool.util;

import java.util.ArrayList;
import java.util.List;

/**
 * List列表工具类
 *
 * @author cliod
 * @date 19-7-23
 * package : com.wobangkj.git.cliod.util
 */
@Deprecated
public class ListUtil {

    private static List<Object> list;
    private static int size;

    /**
     * 列表逻辑分页
     *
     * @param list 总列表
     * @param size 每页的数据条数
     * @param <T>  实体类型
     * @return 页
     */
    public static <T> List<List<T>> splitList(List<T> list, int size) {
//        ListUtil.list.add(list);
//        ListUtil.size = size;
        //listSize -> 总共的数据条数
        int listSize = list.size();
        // page -> 总页数
        int page = (listSize + (size - 1)) / size;

        List<List<T>> listArray = new ArrayList<List<T>>();
        for (int i = 0; i < page; i++) {
            List<T> subList = new ArrayList<T>();
            for (int j = 0; j < listSize; j++) {
                int pageIndex = ((j + 1) + (size - 1)) / size;
                if (pageIndex == (i + 1)) {
                    subList.add(list.get(j));
                }
                if ((j + 1) == ((j + 1) * size)) {
                    break;
                }
            }
            listArray.add(subList);
        }
        return listArray;
    }

    public static <T> List<List<T>> getPages(List<T> list, int size) {
        List<List<T>> result = new ArrayList<>();
        for (int begin = 0; begin < list.size(); begin = begin + size) {
            int end = (begin + size > list.size() ? list.size() : begin + size);
            result.add(list.subList(begin, end));
        }
        return result;
    }

    public static <T> List<T> getPages(List<T> list, int page, int size) {
        if (list.size() <= size) {
            return list;
        }
        return list.subList(page * size, (page + 1) * size);
    }
}
