package com.wobangkj.tool.api.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 页码等数据
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pager {

	private Integer clientPage; //当前页码
	private Integer sumPage;    //数据总数量
	private Integer everyPage;  //每一页显示的数量

}
