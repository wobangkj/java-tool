package com.wobangkj.tool.api.result;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * com.wobangkj.tool
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GetInfo<T> extends MapData {

	private T data;

	public GetInfo() {
	}

	public GetInfo(Integer status, String msg, T data) {
		super.setStatus(status);
		super.setMsg(msg);
		this.data = data;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
