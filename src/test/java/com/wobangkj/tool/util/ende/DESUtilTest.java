package com.wobangkj.tool.util.ende;

/**
 * @author cliod
 * @since 12/30/20 4:03 PM
 */
class DESUtilTest {

	public static void main(String[] args) {
		System.out.println(DESUtil.encrypt("12345", DESUtil.CONST_DES_KEY_1, DESUtil.CONST_DES_KEY_2, DESUtil.CONST_DES_KEY_3));
		System.out.println(DESUtil.encrypt("12345", DESUtil.CONST_DES_KEY_1));
		System.out.println(DESUtil.encrypt("12345", DESUtil.CONST_DES_KEY_2));
		System.out.println(DESUtil.encrypt("12345"));
	}
}